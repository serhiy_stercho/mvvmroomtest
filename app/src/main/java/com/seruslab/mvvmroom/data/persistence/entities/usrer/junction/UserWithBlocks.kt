package com.seruslab.mvvmroom.data.persistence.entities.usrer.junction

import androidx.room.Embedded
import androidx.room.Relation
import com.seruslab.mvvmroom.data.persistence.entities.usrer.User
import com.seruslab.mvvmroom.data.persistence.entities.words.WordBlock

data class UserWithBlocks(
    @Embedded val user: User,
    @Relation(
        parentColumn = "user_id",
        entityColumn = "user_owner_id"
    )
    val blocks: List<WordBlock>
)