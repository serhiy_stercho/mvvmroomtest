package com.seruslab.mvvmroom.data.persistence.base

import androidx.lifecycle.LiveData
import androidx.room.*
import io.reactivex.Completable

typealias BaseReturnStructure = Completable

@Dao
interface BaseDao<in T> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg entities: T) : BaseReturnStructure
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(entities: List<T>) : BaseReturnStructure
    @Update
    fun update(vararg entities: T) : BaseReturnStructure
    @Delete
    fun delete(vararg entities: T) : BaseReturnStructure
}