package com.seruslab.mvvmroom.data.words

import androidx.lifecycle.LiveData
import com.seruslab.mvvmroom.data.persistence.entities.words.Word
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

interface ISaveWordsRepository {

    fun saveWord(word: Word) : Completable
    fun saveAllWords(word: List<Word>) : Completable

    fun getAllWords() : Observable<List<Word>>
    fun getWords() : Single<List<Word>>
}