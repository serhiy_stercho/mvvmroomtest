package com.seruslab.mvvmroom.data.user

import com.seruslab.mvvmroom.data.persistence.entities.usrer.User
import com.seruslab.mvvmroom.presentation.application.WordsApplication
import io.reactivex.Completable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class SaveUserRepository @Inject constructor() : ISaveUserRepository {

    override fun saveUser(user: User): Completable = WordsApplication.getInstance().getDbInstance()?.getUserDao()?.insert(user)
        ?.subscribeOn(Schedulers.io())
        ?.observeOn(Schedulers.io())!!
}