package com.seruslab.mvvmroom.data.word_blocks

import com.seruslab.mvvmroom.data.persistence.entities.words.Word
import com.seruslab.mvvmroom.data.persistence.entities.words.WordBlock
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single


interface IManageWbRepository {
    fun insertWb(wb: WordBlock) : Completable
    fun insertAsSingle(wb: WordBlock) : Single<Long>
    fun getAllWordsInWordBlock(wbId: Long) : Maybe<List<Word>>
}