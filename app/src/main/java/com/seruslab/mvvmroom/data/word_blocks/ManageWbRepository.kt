package com.seruslab.mvvmroom.data.word_blocks

import com.seruslab.mvvmroom.data.persistence.entities.words.Word
import com.seruslab.mvvmroom.data.persistence.entities.words.WordBlock
import com.seruslab.mvvmroom.presentation.application.WordsApplication
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ManageWbRepository @Inject constructor() : IManageWbRepository {

    override fun insertWb(wb: WordBlock): Completable = WordsApplication.getInstance().getDbInstance()
        ?.getWordBlocksDao()
        ?.insert(wb)
        ?.subscribeOn(Schedulers.io())
        ?.observeOn(Schedulers.io())!!

    override fun insertAsSingle(wb: WordBlock): Single<Long> = WordsApplication.getInstance().getDbInstance()
        ?.getWordBlocksDao()
        ?.insertAsSingle(wb)
        ?.subscribeOn(Schedulers.io())
        ?.observeOn(Schedulers.io())!!

    override fun getAllWordsInWordBlock(wbId: Long): Maybe<List<Word>> = WordsApplication.getInstance().getDbInstance()
        ?.getWordBlocksDao()
        ?.getWordBlockById(wbId)
        ?.map { block ->
            return@map block.words
        }
        ?.onErrorReturn { emptyList() }
        ?.subscribeOn(Schedulers.io())
        ?.observeOn(Schedulers.io())!!

}