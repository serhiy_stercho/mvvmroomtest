package com.seruslab.mvvmroom.data.persistence.daos

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import com.seruslab.mvvmroom.data.persistence.base.BaseDao
import com.seruslab.mvvmroom.data.persistence.entities.usrer.User
import com.seruslab.mvvmroom.data.persistence.entities.usrer.junction.UserWithBlocks
import com.seruslab.mvvmroom.data.persistence.entities.usrer.junction.UsersAndBlocks
import io.reactivex.Observable

@Dao
interface UserDao : BaseDao<User> {
    @Transaction
    @Query("SELECT * FROM users")
    fun getUserWithBlocks() : Observable<UserWithBlocks>

    @Transaction
    @Query("SELECT * FROM users")
    fun getUsersAndBlocks() : Observable<UsersAndBlocks>
}