package com.seruslab.mvvmroom.data.persistence.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.seruslab.mvvmroom.data.persistence.daos.UserDao
import com.seruslab.mvvmroom.data.persistence.daos.WordBlockDao
import com.seruslab.mvvmroom.data.persistence.daos.WordsDao
import com.seruslab.mvvmroom.data.persistence.entities.usrer.User
import com.seruslab.mvvmroom.data.persistence.entities.words.Word
import com.seruslab.mvvmroom.data.persistence.entities.words.WordBlock

@Database(entities = arrayOf(Word::class, WordBlock::class, User::class), version = 4)
abstract class AppDatabase : RoomDatabase() {

    abstract fun getWordsDao() : WordsDao
    abstract fun getWordBlocksDao() : WordBlockDao
    abstract fun getUserDao() : UserDao

    companion object {
        val DB_NAME = "APP_DATABASE"
        var instance: AppDatabase? = null
        fun getInstance(context: Context): AppDatabase? {
            if (instance == null) {
                synchronized(AppDatabase::class) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        AppDatabase::class.java,
                        DB_NAME
                    ).fallbackToDestructiveMigration()
                        .build()
                }
            }

            return instance
        }

    }
}