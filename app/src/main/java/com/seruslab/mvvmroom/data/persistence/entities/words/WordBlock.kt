package com.seruslab.mvvmroom.data.persistence.entities.words

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "word_blocks")
data class WordBlock(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "block_id")
    val id: Int? = null,
    @ColumnInfo(name = "user_owner_id")
    val userOwnerId: Long? = null,
    @ColumnInfo(name = "block_name")
    var wordBlockName: String
)