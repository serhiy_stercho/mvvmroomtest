package com.seruslab.mvvmroom.data.persistence.daos

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.seruslab.mvvmroom.data.persistence.base.BaseDao
import com.seruslab.mvvmroom.data.persistence.entities.words.Word
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

@Dao
interface WordsDao : BaseDao<Word> {


    @Query("SELECT * FROM words")
    fun getAllWords() : Observable<List<Word>>

    @Query("SELECT * FROM words")
    fun getWords() : Single<List<Word>>
}