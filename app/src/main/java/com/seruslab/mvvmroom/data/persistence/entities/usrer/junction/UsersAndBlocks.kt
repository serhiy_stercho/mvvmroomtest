package com.seruslab.mvvmroom.data.persistence.entities.usrer.junction

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation
import com.seruslab.mvvmroom.data.persistence.entities.usrer.User
import com.seruslab.mvvmroom.data.persistence.entities.words.WordBlock

data class UsersAndBlocks(
    @Embedded val user: User,
    @Relation(
        parentColumn = "user_id",
        entityColumn = "block_id",
        associateBy = Junction(UserBlockCrossRef::class)
    )
    val blocks: List<WordBlock>
)