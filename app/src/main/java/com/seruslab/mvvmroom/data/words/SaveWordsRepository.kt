package com.seruslab.mvvmroom.data.words

import android.util.Log
import androidx.lifecycle.LiveData
import com.seruslab.mvvmroom.data.persistence.entities.words.Word
import com.seruslab.mvvmroom.presentation.application.WordsApplication
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class SaveWordsRepository @Inject constructor() : ISaveWordsRepository {

    override fun saveWord(word: Word): Completable {
        return WordsApplication.getInstance().getDbInstance()?.getWordsDao()
                ?.insert(word)!!
    }

    override fun saveAllWords(words: List<Word>): Completable {
        return WordsApplication.getInstance().getDbInstance()?.getWordsDao()
            ?.insertAll(words)!!
    }

    override fun getAllWords(): Observable<List<Word>> {
        return WordsApplication.getInstance().getDbInstance()?.getWordsDao()?.getAllWords()
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(Schedulers.io())!!
    }

    override fun getWords(): Single<List<Word>> {
        return WordsApplication.getInstance().getDbInstance()?.getWordsDao()?.getWords()
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(Schedulers.io())!!
    }


}