package com.seruslab.mvvmroom.data.persistence.entities.words

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "words")
data class Word(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "word_id")
    val id: Long? = null,
    @ColumnInfo(name = "word_owner_id")
    val wordOwnerId: Long? = null,
    val original: String,
    val translation: String
)