package com.seruslab.mvvmroom.data.persistence.daos

import androidx.lifecycle.LiveData
import androidx.room.*
import com.seruslab.mvvmroom.data.persistence.base.BaseDao
import com.seruslab.mvvmroom.data.persistence.entities.words.WordBlock
import com.seruslab.mvvmroom.data.persistence.entities.words.junctions.BlockWithWords
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single


@Dao
interface WordBlockDao : BaseDao<WordBlock> {

    @Transaction
    @Query("SELECT * FROM word_blocks")
    fun getAllWordBlocks() : Observable<List<BlockWithWords>>

    @Transaction
    @Query("SELECT * FROM word_blocks WHERE block_id = :id")
    fun getWordBlockById(id: Long) : Maybe<BlockWithWords>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAsSingle(block: WordBlock) : Single<Long>
}