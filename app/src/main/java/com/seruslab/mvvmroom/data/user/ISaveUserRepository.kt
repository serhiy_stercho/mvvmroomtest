package com.seruslab.mvvmroom.data.user

import com.seruslab.mvvmroom.data.persistence.entities.usrer.User
import io.reactivex.Completable

interface ISaveUserRepository {
    fun saveUser(user: User) : Completable
}