package com.seruslab.mvvmroom.data.persistence.entities.usrer.junction

import androidx.room.Entity

@Entity(primaryKeys = ["user_id","block_id"])
data class UserBlockCrossRef(
    val userId: Long,
    val blockId: Long
)