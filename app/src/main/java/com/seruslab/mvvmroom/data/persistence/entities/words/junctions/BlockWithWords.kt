package com.seruslab.mvvmroom.data.persistence.entities.words.junctions

import androidx.room.Embedded
import androidx.room.Relation
import com.seruslab.mvvmroom.data.persistence.entities.words.Word
import com.seruslab.mvvmroom.data.persistence.entities.words.WordBlock

data class BlockWithWords(
    @Embedded
    val block: WordBlock,
    @Relation(
        parentColumn = "block_id",
        entityColumn = "word_owner_id"
    )
    val words: List<Word>
)