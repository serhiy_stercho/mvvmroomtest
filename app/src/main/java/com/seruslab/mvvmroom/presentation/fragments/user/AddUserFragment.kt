package com.seruslab.mvvmroom.presentation.fragments.user

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.jakewharton.rxbinding2.widget.RxTextView
import com.seruslab.mvvmroom.R
import com.seruslab.mvvmroom.databinding.FragmentAddUserBinding
import com.seruslab.mvvmroom.di.Injectable
import dagger.android.support.AndroidSupportInjection
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class AddUserFragment : Fragment(), Injectable {

    @Inject
    lateinit var mFactory: ViewModelProvider.Factory

    private lateinit var mBinding: FragmentAddUserBinding

    private lateinit var mViewModel: AddUserViewModel

    private val compositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onCreate(savedInstanceState)
        obtainViewModel()
    }

    private fun obtainViewModel() {
        mViewModel = ViewModelProvider(this, mFactory)
            .get(AddUserViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_add_user,
            container,
            false)
        mBinding.viewModel = mViewModel

        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupStream()
    }

    private fun setupStream() {
        RxTextView.textChangeEvents(mBinding.edtFirstNameFAU)
            .share()
            .doOnNext {event ->
                mViewModel.setupFirstName(event.text().toString())
            }
            .subscribe()
            .let {
                compositeDisposable.add(it)
            }

        RxTextView.textChangeEvents(mBinding.edtLastNameFAU)
            .share()
            .doOnNext {event ->
                mViewModel.setupLastName(event.text().toString())
            }
            .subscribe()
            .let {
                compositeDisposable.add(it)
            }
    }

    companion object {

        fun newInstance() = AddUserFragment().apply {
            arguments = Bundle().apply {  }
        }
    }
}