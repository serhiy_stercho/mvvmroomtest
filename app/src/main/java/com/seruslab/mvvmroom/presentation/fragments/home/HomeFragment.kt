package com.seruslab.mvvmroom.presentation.fragments.home

import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatEditText
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.jakewharton.rxbinding2.view.RxView
import com.jakewharton.rxbinding2.widget.RxTextView
import com.seruslab.mvvmroom.R
import com.seruslab.mvvmroom.databinding.FragmentHomeBinding
import com.seruslab.mvvmroom.di.Injectable
import com.seruslab.mvvmroom.presentation.application.WordsApplication
import com.seruslab.mvvmroom.presentation.fragments.newWordBlock.FragmentNewWordBlock
import com.seruslab.mvvmroom.presentation.fragments.user.AddUserFragment
import dagger.android.AndroidInjection
import dagger.android.support.AndroidSupportInjection
import io.reactivex.disposables.CompositeDisposable
import java.lang.StringBuilder

import javax.inject.Inject

class HomeFragment : Fragment(), Injectable {

    @Inject
    lateinit var mFactory: ViewModelProvider.Factory

    lateinit var mBinding: FragmentHomeBinding
    lateinit var mViewModel: HomeViewModel

    private val compositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onCreate(savedInstanceState)
        obtainViewModel()
    }

    private fun obtainViewModel() {
        mViewModel = ViewModelProvider(this, mFactory)
            .get(HomeViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_home,
            container,
            false
        )
        mBinding.viewModel = mViewModel

        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        registerWordsObserver()
        setupStream()
    }

    private fun registerWordsObserver() {
        mViewModel.allWords.observe(this, Observer { words ->
            words?.forEach {
                mBinding.tvWordsListFH.text = StringBuilder(mBinding.tvWordsListFH.text)
                    .append("\n")
                    .append(it.original)
                    .append(" - ")
                    .append(it.translation)
                    .toString()
            }
        })
    }

    private fun setupStream() {
        val originalChanges = RxTextView.textChangeEvents(mBinding.edtOriginalFH)
            .share()
            .doOnNext { event ->
                mViewModel.setupOriginal(event.text().toString())
            }
            .subscribe()

        val translationChanges = RxTextView.textChangeEvents(mBinding.edtTranslationFH)
            .share()
            .doOnNext { event ->
                mViewModel.setupTranslation(event.text().toString())
            }
            .subscribe()

        val wordOwnerIdChanges = RxTextView.textChangeEvents(mBinding.edtWbIDFH)
            .share()
            .doOnNext { event ->
                if (!event.text().toString().isNullOrEmpty())
                    mViewModel.setupWordOwner(event.text().toString().toLong())
            }
            .subscribe()

        val createWordBlock = RxView.clicks(mBinding.btnCreateWordBlockFH)
            .share()
            .doOnNext {
                createWbDialog()
            }
            .subscribe()

        val createBlockWithWords = RxView.clicks(mBinding.btnCreateBlockAlreadyFH)
            .share()
            .doOnNext {
                addFragment(FragmentNewWordBlock())
            }
            .subscribe()

        val addUser= RxView.clicks(mBinding.btnAddUserFH)
            .share()
            .doOnNext {event ->  addFragment(AddUserFragment.newInstance())}
            .subscribe()


        compositeDisposable.addAll(
            originalChanges,
            translationChanges,
            createWordBlock,
            wordOwnerIdChanges,
            createBlockWithWords,
            addUser
        )
    }

    private fun createWbDialog() {
        val alertDialogBuilder = AlertDialog.Builder(context!!)
        val inflater = requireActivity().layoutInflater
        val view = inflater.inflate(R.layout.item_create_wb_dialog, null, false)
        alertDialogBuilder.setView(view)
            .setPositiveButton("Ok", { dialog, which ->
                val blockName = view.findViewById<AppCompatEditText>(R.id.edtWbName_ICBD)
                    .text?.toString() ?: "def"

                mViewModel.createWordBlock(blockName)
            })
            .setNegativeButton("Cancel", { dialog, which ->
                dialog.cancel()
            })

        alertDialogBuilder.show()
    }

    private fun addFragment(fragment: Fragment) {
        fragmentManager?.beginTransaction()
            ?.replace(R.id.flContainer_AM, fragment)
            ?.commit()
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }
}