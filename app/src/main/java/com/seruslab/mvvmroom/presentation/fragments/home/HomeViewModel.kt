package com.seruslab.mvvmroom.presentation.fragments.home

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.seruslab.mvvmroom.data.persistence.entities.words.Word
import com.seruslab.mvvmroom.data.persistence.entities.words.WordBlock
import com.seruslab.mvvmroom.domain.usecases.word_blocks.GetWordsWithinBlock
import com.seruslab.mvvmroom.domain.usecases.word_blocks.ICreateWordBlockUseCase
import com.seruslab.mvvmroom.domain.usecases.word_blocks.IGetWordsWithinBlock
import com.seruslab.mvvmroom.domain.usecases.words.IGetAllWordsUseCase
import com.seruslab.mvvmroom.domain.usecases.words.ISaveWordsUseCase
import com.seruslab.mvvmroom.presentation.application.WordsApplication
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class HomeViewModel @Inject constructor(private val iSaveWordsUseCase: ISaveWordsUseCase,
                                        private val iGetAllWordsUseCase: IGetAllWordsUseCase,
                                        private val iCreateWordBlockUseCase: ICreateWordBlockUseCase,
                                        private val iGetWordsWithinBlock: IGetWordsWithinBlock) : ViewModel() {

    private val original = MutableLiveData<String>()
    private val translation= MutableLiveData<String>()
    private val wordOwnerId= MutableLiveData<Long>()
    private val blockName = MutableLiveData<String>()
    private val blockOwnerId = MutableLiveData<Long>()

    val allWords = MutableLiveData<List<Word>>()

    private val compositeDisposable = CompositeDisposable()

    init {
        original.value = ""
        translation.value = ""
    }

    fun saveData() {
        val saveWord = iSaveWordsUseCase.execute(Word(original = original.value!!,
            translation = translation.value!!,
            wordOwnerId = wordOwnerId.value ?: 1))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                Log.i("Save", "Success")
            },{
                Log.i("Save", it.message)
            })

        compositeDisposable.add(saveWord)
    }

    fun getWords() {
        val getAllWords = iGetAllWordsUseCase.execute()
            .subscribe({words ->
                allWords.value = words
            },{
                Log.i("Save", it.message)
            })

        compositeDisposable.add(getAllWords)
    }

    fun createWordBlockAndPushWords(words: List<Word>) {
        iCreateWordBlockUseCase.createAsSingle(WordBlock(wordBlockName = blockName?.value!!, userOwnerId = blockOwnerId.value!!))
            .flatMapCompletable {blockId ->
                iSaveWordsUseCase.putWordsArray(words.map { it.copy(wordOwnerId = blockId) })
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                Log.i("Save", "Success")
            },{
                Log.i("Save", it.message)
            })
            .let { compositeDisposable.add(it) }
    }

    fun createWordBlock(name: String) {
        iCreateWordBlockUseCase.execute(WordBlock(wordBlockName = name))
            .subscribe({
                Log.i("Save", "Success")
            },{
                Log.i("Save", it.message)
            }).let {
                compositeDisposable.add(it)
            }
    }

    fun getWordsInBlock() {
        iGetWordsWithinBlock.execute(wordOwnerId.value ?: 1)
            .subscribe({
                allWords.value = it
            }, {
                Log.i("Save", it.message)
            }, {

            }).let {
                compositeDisposable.add(it)
            }
    }

    fun setupWordOwner(_ownerId: Long) {
        wordOwnerId.value = _ownerId
    }

    fun setupBlockOwner(_blockOwnerId: Long) {
        blockOwnerId.value = _blockOwnerId
    }

    fun setupOriginal(value: String) {
        original.value = value
    }

    fun setupTranslation(value: String) {
        translation.value = value
    }

    fun setupWordBlockName(_name: String) {
        blockName.value = _name
    }

}