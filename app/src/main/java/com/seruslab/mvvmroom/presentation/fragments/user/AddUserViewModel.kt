package com.seruslab.mvvmroom.presentation.fragments.user

import android.os.Bundle
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.seruslab.mvvmroom.data.persistence.entities.usrer.User
import com.seruslab.mvvmroom.domain.usecases.user.IStoreUserUseCase
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class AddUserViewModel @Inject constructor(private val iStoreUserUseCase: IStoreUserUseCase) : ViewModel() {

    private val firstName = MutableLiveData<String>()
    private val lastName = MutableLiveData<String>()

    private val compositeDisposable = CompositeDisposable()

    init {
        firstName.value = ""
        lastName.value = ""
    }

    fun setupFirstName(value: String) {
        firstName.value = value
    }

    fun setupLastName(value: String) {
        lastName.value = value
    }

    fun createUser() {
        if(firstName.value?.isEmpty()!! && lastName.value?.isEmpty()!!)
            return

        iStoreUserUseCase.execute(User(firstName = firstName.value!!, lastName = lastName.value!!))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                Log.i("tag", "Success")
            },{
                Log.i("tag", "Error")
            })
            .let {
                compositeDisposable.add(it)
            }
    }


}