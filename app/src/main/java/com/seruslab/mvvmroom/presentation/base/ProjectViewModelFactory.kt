package com.seruslab.mvvmroom.presentation.base

import android.provider.ContactsContract
import android.util.ArrayMap
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.seruslab.mvvmroom.di.ViewModelSubComponent
import com.seruslab.mvvmroom.presentation.fragments.home.HomeViewModel
import com.seruslab.mvvmroom.presentation.fragments.user.AddUserViewModel
import java.lang.Exception
import java.lang.IllegalArgumentException
import java.lang.RuntimeException
import java.util.concurrent.Callable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ProjectViewModelFactory @Inject constructor(val viewModelSubComponent: ViewModelSubComponent) : ViewModelProvider.Factory {

    private val creators: ArrayMap<Class<*>, Callable<out ViewModel>> = ArrayMap()

    init {
        creators.put(HomeViewModel::class.java, Callable { viewModelSubComponent.provideHomeViewModel() } )
        creators.put(AddUserViewModel::class.java, Callable { viewModelSubComponent.provideAddUserViewModel() } )
    }

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        var creator: Callable<out ViewModel>? = creators[modelClass]

        creator?.let {
            creators.entries.forEach { entry ->
                if(modelClass.isAssignableFrom(entry.key)){
                    creator = entry.value
                    return@forEach
                }
            }
        } ?: throw IllegalArgumentException("Unknown model class $modelClass")

        try {
           return creator!!.call() as T
        } catch (e: Exception) {
            throw RuntimeException(e)
        }
    }

}