package com.seruslab.mvvmroom.presentation.application

import android.app.Activity
import android.app.Application
import com.facebook.stetho.Stetho
import com.seruslab.mvvmroom.data.persistence.database.AppDatabase
import com.seruslab.mvvmroom.di.AppInjector
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

class WordsApplication : Application(), HasAndroidInjector {

    //private lateinit var mAppComponent: AppComponent

    @Inject
    lateinit var mActivityInjector: DispatchingAndroidInjector<Any>

    override fun onCreate() {
        super.onCreate()
        instance = this
        AppInjector.init(this)
        Stetho.initializeWithDefaults(this)
    }

    fun getDbInstance() = AppDatabase.getInstance(this)

    companion object {
        private lateinit var instance: WordsApplication

        fun getInstance() = instance
    }

    override fun androidInjector(): AndroidInjector<Any> = mActivityInjector
}