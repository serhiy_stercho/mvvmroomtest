package com.seruslab.mvvmroom.presentation.fragments.newWordBlock

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatEditText
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.jakewharton.rxbinding2.view.RxView
import com.jakewharton.rxbinding2.widget.RxTextView
import com.seruslab.mvvmroom.R
import com.seruslab.mvvmroom.data.persistence.entities.words.Word
import com.seruslab.mvvmroom.databinding.FragmentNewWordblockBinding
import com.seruslab.mvvmroom.di.Injectable
import com.seruslab.mvvmroom.presentation.fragments.home.HomeViewModel
import dagger.android.support.AndroidSupportInjection
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class FragmentNewWordBlock : Fragment(), Injectable {

    @Inject
    lateinit var mFactory: ViewModelProvider.Factory

    private lateinit var mBinding: FragmentNewWordblockBinding
    private lateinit var mViewModel: HomeViewModel

    private val compositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onCreate(savedInstanceState)
        obtainViewModel()
    }

    private fun obtainViewModel() {
        mViewModel = ViewModelProvider(this, mFactory)
            .get(HomeViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_new_wordblock, container, false)
        mBinding.viewModel = mViewModel

        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupStream()
    }

    private fun setupStream() {
        RxView.clicks(mBinding.btnAddTranslationFNWB)
            .share()
            .doOnNext { addWordToLayout() }
            .subscribe()
            .let {
                compositeDisposable.add(it)
            }

        RxTextView.textChangeEvents(mBinding.edtUserOwnerIDFNWB)
            .share()
            .filter {_event -> _event.text().isNotEmpty()}
            .doOnNext {
                mViewModel.setupBlockOwner(it.text().toString().toLong())
            }
            .subscribe()
            .let {
                compositeDisposable.add(it)
            }

        RxTextView.textChangeEvents(mBinding.edtBlockNameFNWB)
            .share()
            .doOnNext { event ->
                mViewModel.setupWordBlockName(event.text().toString())
            }
            .subscribe()
            .let {
                compositeDisposable.add(it)
            }

        RxView.clicks(mBinding.btnCreateBlockFNWB)
            .share()
            .doOnNext { collectWords() }
            .subscribe()
            .let {
                compositeDisposable.add(it)
            }
    }

    private fun addWordToLayout() {
        val view =
            LayoutInflater.from(context).inflate(R.layout.item_original_translation, null, false)
        view.findViewById<ImageView>(R.id.ivDeleteRow_IOT).setOnClickListener {
            mBinding.llWordsContainerFNWB.removeView(view)
        }

        mBinding.llWordsContainerFNWB.addView(view)
    }

    private fun collectWords() {
        val words = arrayListOf<Word>()
        for (index in 0 until mBinding.llWordsContainerFNWB.childCount) {
            val original = mBinding.llWordsContainerFNWB.getChildAt(index)
                .findViewById<AppCompatEditText>(R.id.edtOriginal_IOT).text.toString()

            val translation = mBinding.llWordsContainerFNWB.getChildAt(index)
                .findViewById<AppCompatEditText>(R.id.edtTranslation_IOT).text.toString()
            words.add(Word(original = original, translation = translation))
        }

        mViewModel.createWordBlockAndPushWords(words)
    }
}