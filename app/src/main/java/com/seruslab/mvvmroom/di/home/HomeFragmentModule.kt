package com.seruslab.mvvmroom.di.home

import androidx.lifecycle.ViewModel
import com.seruslab.mvvmroom.data.word_blocks.IManageWbRepository
import com.seruslab.mvvmroom.data.word_blocks.ManageWbRepository
import com.seruslab.mvvmroom.data.words.ISaveWordsRepository
import com.seruslab.mvvmroom.data.words.SaveWordsRepository
import com.seruslab.mvvmroom.domain.usecases.word_blocks.CreateWordBlockUseCase
import com.seruslab.mvvmroom.domain.usecases.word_blocks.GetWordsWithinBlock
import com.seruslab.mvvmroom.domain.usecases.word_blocks.ICreateWordBlockUseCase
import com.seruslab.mvvmroom.domain.usecases.word_blocks.IGetWordsWithinBlock
import com.seruslab.mvvmroom.domain.usecases.words.GetAllWordsUseCase
import com.seruslab.mvvmroom.domain.usecases.words.IGetAllWordsUseCase
import com.seruslab.mvvmroom.domain.usecases.words.ISaveWordsUseCase
import com.seruslab.mvvmroom.domain.usecases.words.SaveWordsUseCase
import com.seruslab.mvvmroom.presentation.fragments.home.HomeViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class HomeFragmentModule {

    @Binds
    abstract fun provideViewModel(viewModel: HomeViewModel) : ViewModel

    @Binds
    abstract fun provideSaveWordsUseCase(useCase: SaveWordsUseCase) : ISaveWordsUseCase

    @Binds
    abstract fun provideGetWordsUseCase(useCase: GetAllWordsUseCase) : IGetAllWordsUseCase

    @Binds
    abstract fun provideCreateWbUseCase(useCase: CreateWordBlockUseCase) : ICreateWordBlockUseCase

    @Binds
    abstract fun provideGetWordsInBlockUseCase(useCase: GetWordsWithinBlock) : IGetWordsWithinBlock

    @Binds
    abstract fun provideManageWbRepository(useCase: ManageWbRepository) : IManageWbRepository

    @Binds
    abstract fun provideSaveWordsRepository(repo: SaveWordsRepository) : ISaveWordsRepository
}