package com.seruslab.mvvmroom.di

import androidx.lifecycle.ViewModelProvider
import com.seruslab.mvvmroom.di.home.HomeFragmentModule
import com.seruslab.mvvmroom.di.user.UserFragmentModule
import com.seruslab.mvvmroom.presentation.base.ProjectViewModelFactory
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(subcomponents = [ViewModelSubComponent::class], includes = [HomeFragmentModule::class, UserFragmentModule::class])
class AppModule {

    @Singleton
    @Provides
    fun provideViewModelFactory(viewModelSubComponent: ViewModelSubComponent.Builder) : ViewModelProvider.Factory =
        ProjectViewModelFactory(viewModelSubComponent.build())
}