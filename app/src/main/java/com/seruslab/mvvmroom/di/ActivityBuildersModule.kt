package com.seruslab.mvvmroom.di

import com.seruslab.mvvmroom.di.home.HomeFragmentModule
import com.seruslab.mvvmroom.presentation.activities.main.MainActivity
import com.seruslab.mvvmroom.presentation.basics.annotations.PerActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuildersModule {
    @ContributesAndroidInjector(modules = [FragmentBuildersModule::class])
    abstract fun contributeMainActivity() : MainActivity
}