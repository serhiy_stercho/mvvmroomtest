package com.seruslab.mvvmroom.di

import com.seruslab.mvvmroom.di.home.HomeFragmentModule
import com.seruslab.mvvmroom.di.newWordBlock.NewWordBlockFragmentModule
import com.seruslab.mvvmroom.di.user.UserFragmentModule
import com.seruslab.mvvmroom.presentation.fragments.home.HomeFragment
import com.seruslab.mvvmroom.presentation.fragments.newWordBlock.FragmentNewWordBlock
import com.seruslab.mvvmroom.presentation.fragments.user.AddUserFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuildersModule {
    @ContributesAndroidInjector(modules = [HomeFragmentModule::class])
    abstract fun contributeHomeFragment() : HomeFragment

    @ContributesAndroidInjector(modules = [NewWordBlockFragmentModule::class])
    abstract fun contributeNewWordBlockFragment() : FragmentNewWordBlock

    @ContributesAndroidInjector(modules = [UserFragmentModule::class])
    abstract fun contributeAddUserFragment() : AddUserFragment
}