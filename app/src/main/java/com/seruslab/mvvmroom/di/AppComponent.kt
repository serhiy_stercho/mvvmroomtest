package com.seruslab.mvvmroom.di

import android.app.Application
import com.seruslab.mvvmroom.presentation.application.WordsApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AppModule::class,
    AndroidInjectionModule::class,
    ActivityBuildersModule::class,
    FragmentBuildersModule::class
])
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance fun application(application: Application) : Builder
        fun build() : AppComponent
    }

    fun inject(wordsApp: WordsApplication)
}