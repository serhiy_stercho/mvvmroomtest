package com.seruslab.mvvmroom.di.user

import androidx.lifecycle.ViewModel
import com.seruslab.mvvmroom.data.user.ISaveUserRepository
import com.seruslab.mvvmroom.data.user.SaveUserRepository
import com.seruslab.mvvmroom.domain.usecases.user.IStoreUserUseCase
import com.seruslab.mvvmroom.domain.usecases.user.StoreUserUseCase
import com.seruslab.mvvmroom.presentation.fragments.user.AddUserViewModel
import dagger.Binds
import dagger.Module

@Module
abstract class UserFragmentModule {
    @Binds
    abstract fun provideViewModel(viewModel: AddUserViewModel) : ViewModel

    @Binds
    abstract fun provideStoreUserUseCase(storeUserUseCase: StoreUserUseCase) : IStoreUserUseCase

    @Binds
    abstract fun provideSaveUserRepository(repository: SaveUserRepository) : ISaveUserRepository
}