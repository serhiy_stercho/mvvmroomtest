package com.seruslab.mvvmroom.di

import com.seruslab.mvvmroom.presentation.fragments.home.HomeViewModel
import com.seruslab.mvvmroom.presentation.fragments.user.AddUserViewModel
import dagger.Subcomponent

@Subcomponent
interface ViewModelSubComponent {

    @Subcomponent.Builder
    interface Builder {
        fun build() : ViewModelSubComponent
    }

    fun provideHomeViewModel() : HomeViewModel
    fun provideAddUserViewModel() : AddUserViewModel
}