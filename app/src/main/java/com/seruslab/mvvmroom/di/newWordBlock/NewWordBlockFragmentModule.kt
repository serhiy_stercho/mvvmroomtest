package com.seruslab.mvvmroom.di.newWordBlock

import com.seruslab.mvvmroom.di.home.HomeFragmentModule
import dagger.Module

@Module(includes = [HomeFragmentModule::class])
class NewWordBlockFragmentModule {

}