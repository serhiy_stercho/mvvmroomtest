package com.seruslab.mvvmroom.domain.usecases.user

import com.seruslab.mvvmroom.data.persistence.entities.usrer.User
import io.reactivex.Completable

interface IStoreUserUseCase {
    fun execute(user: User) : Completable
}