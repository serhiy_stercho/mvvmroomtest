package com.seruslab.mvvmroom.domain.usecases.word_blocks

import com.seruslab.mvvmroom.data.persistence.entities.words.WordBlock
import com.seruslab.mvvmroom.data.word_blocks.IManageWbRepository
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class CreateWordBlockUseCase @Inject constructor(private val iManageWbRepository: IManageWbRepository) :  ICreateWordBlockUseCase {

    override fun execute(wb: WordBlock): Completable = iManageWbRepository.insertWb(wb)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())

    override fun createAsSingle(wb: WordBlock): Single<Long> = iManageWbRepository.insertAsSingle(wb)
}