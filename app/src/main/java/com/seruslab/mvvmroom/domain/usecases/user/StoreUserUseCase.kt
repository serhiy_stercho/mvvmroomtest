package com.seruslab.mvvmroom.domain.usecases.user

import com.seruslab.mvvmroom.data.persistence.entities.usrer.User
import com.seruslab.mvvmroom.data.user.ISaveUserRepository
import io.reactivex.Completable
import javax.inject.Inject

class StoreUserUseCase @Inject constructor(private val iSaveUserRepository: ISaveUserRepository) : IStoreUserUseCase {

    override fun execute(user: User): Completable = iSaveUserRepository.saveUser(user)
}