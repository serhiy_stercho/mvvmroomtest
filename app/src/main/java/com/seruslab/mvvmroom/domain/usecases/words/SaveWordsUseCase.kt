package com.seruslab.mvvmroom.domain.usecases.words

import com.seruslab.mvvmroom.data.persistence.entities.words.Word
import com.seruslab.mvvmroom.data.words.ISaveWordsRepository
import io.reactivex.Completable
import io.reactivex.ObservableTransformer
import io.reactivex.android.schedulers.AndroidSchedulers

import io.reactivex.schedulers.Schedulers

import javax.inject.Inject

class SaveWordsUseCase @Inject constructor(
    val iSaveWordsRepository: ISaveWordsRepository
) : ISaveWordsUseCase {

    override fun execute(word: Word): Completable {
        return iSaveWordsRepository.saveWord(word)
    }

    override fun putWordsArray(words: List<Word>): Completable {
        return iSaveWordsRepository.saveAllWords(words)
    }

}