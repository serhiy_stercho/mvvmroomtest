package com.seruslab.mvvmroom.domain.usecases.words

import com.seruslab.mvvmroom.data.persistence.entities.words.Word
import io.reactivex.Completable

interface ISaveWordsUseCase {
    fun execute(word: Word) : Completable
    fun putWordsArray(words: List<Word>) : Completable
}