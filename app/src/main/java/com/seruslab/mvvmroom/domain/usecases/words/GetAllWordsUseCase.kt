package com.seruslab.mvvmroom.domain.usecases.words

import androidx.lifecycle.LiveData
import com.seruslab.mvvmroom.data.persistence.entities.words.Word
import com.seruslab.mvvmroom.data.words.ISaveWordsRepository
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class GetAllWordsUseCase @Inject constructor(private val iSaveWordsRepository: ISaveWordsRepository) : IGetAllWordsUseCase {

    override fun execute(): Observable<List<Word>> {
        return iSaveWordsRepository.getAllWords()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    }

    override fun getAll(): Single<List<Word>> {
        return iSaveWordsRepository.getWords()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

}