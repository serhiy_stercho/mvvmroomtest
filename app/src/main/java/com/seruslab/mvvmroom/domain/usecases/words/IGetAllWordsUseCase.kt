package com.seruslab.mvvmroom.domain.usecases.words

import androidx.lifecycle.LiveData
import com.seruslab.mvvmroom.data.persistence.entities.words.Word
import io.reactivex.Observable
import io.reactivex.Single

interface IGetAllWordsUseCase {

    fun execute() : Observable<List<Word>>
    fun getAll() : Single<List<Word>>
}