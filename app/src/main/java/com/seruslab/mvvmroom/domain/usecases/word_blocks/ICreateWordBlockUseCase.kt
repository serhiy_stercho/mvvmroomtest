package com.seruslab.mvvmroom.domain.usecases.word_blocks

import com.seruslab.mvvmroom.data.persistence.entities.words.WordBlock
import io.reactivex.Completable
import io.reactivex.Single

interface ICreateWordBlockUseCase {
    fun execute(wb: WordBlock) : Completable
    fun createAsSingle(wb: WordBlock) : Single<Long>
}