package com.seruslab.mvvmroom.domain.usecases.word_blocks

import com.seruslab.mvvmroom.data.persistence.entities.words.Word
import io.reactivex.Maybe

interface IGetWordsWithinBlock {
    fun execute(wbId: Long) : Maybe<List<Word>>
}