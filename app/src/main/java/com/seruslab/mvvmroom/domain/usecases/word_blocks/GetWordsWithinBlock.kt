package com.seruslab.mvvmroom.domain.usecases.word_blocks

import com.seruslab.mvvmroom.data.persistence.entities.words.Word
import com.seruslab.mvvmroom.data.word_blocks.IManageWbRepository
import io.reactivex.Maybe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class GetWordsWithinBlock @Inject constructor(private val iManageWbRepository: IManageWbRepository): IGetWordsWithinBlock {

    override fun execute(wbId: Long): Maybe<List<Word>> {
        return iManageWbRepository.getAllWordsInWordBlock(wbId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

}